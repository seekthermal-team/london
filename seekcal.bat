echo off
REM
REM This DOS batch script installs the sensor calibration data using
REM sputil to read the telemetry line from the sensor
REM and then parse out the 48-bit ChipId into the CHIPID env variable
REM
if .%1 == . goto default
set SPIDEV=%1
goto skipset
:default
set SPIDEV=spidev1.0
:skipset
REM
REM 1. read ROIC & CHIPID from sensor using spiutil
REM 2. create tar file from CHIPID directory
REM 3. create /sdcard/cal folder
REM 4. push tar file to /sdcard/cal
REM 5. remove frame*.raw
REM 6. extract tar file to folder
REM 7. write CmdWord.bin & RDAC.bin to Maxim using spiutil
REM 8. remove tar file
REM
adb shell /data/local/tmp/spiutil -w416 /dev/%SPIDEV% > telem.txt
set /p TELEM=<telem.txt
set ROIC=%TELEM:~2,2%%TELEM:~0,2%
set CHIP0=%TELEM:~336,2%
set CHIP1=%TELEM:~340,2%
set CHIP2=%TELEM:~344,2%
set CHIP3=%TELEM:~348,2%
set CHIP4=%TELEM:~352,2%
set CHIP5=%TELEM:~356,2%
set PRODUCT=%TELEM:~192,2%
set VARIANT=%TELEM:~196,2%
set MAJOR=%TELEM:~200,2%
set MINOR=%TELEM:~204,2%
set CHIPID=%CHIP5%%CHIP4%%CHIP3%%CHIP2%%CHIP1%%CHIP0%
echo 1. read ROIC is %ROIC%  Version is %PRODUCT%.%VARIANT%.%MAJOR%.%MINOR%  ChipId is %CHIPID%

set SEEKNAS=\\192.168.201.106\nasseek-home\Public\Projects\Microcore\bin
echo NAS Path=%SEEKNAS%

if EXIST "%CHIPID%\" goto skipcopy
    echo ERROR Directory %CHIPID% does not exist!
    if EXIST "%SEEKNAS%\%CHIPID%\" goto copydata
        echo ERROR Directory %SEEKNAS%\%CHIPID% does not exist!
        goto exit
:copydata
	echo Copying caldata fromo %SEEKNAS%\%CHIPID%
    mkdir %CHIPID%
	xcopy %SEEKNAS%\%CHIPID% %CHIPID%
:skipcopy

echo 2. Creating %CHIPID%.tar...
rm -f %CHIPID%.tar
tar -cf %CHIPID%.tar %CHIPID%/AthermLo.bin %CHIPID%/Atherm.bin %CHIPID%/AthermHi.bin %CHIPID%/CintLut.bin %CHIPID%/CmdWord.bin %CHIPID%/RDAC.bin %CHIPID%/FactorySettings.bin %CHIPID%/HG_Delta.bin %CHIPID%/LG_Delta.bin %CHIPID%/ThermAdjust.bin %CHIPID%/ThermAdjust_FF.bin %CHIPID%/ThermHgKa.bin %CHIPID%/ThermHgKm.bin %CHIPID%/IP_SBNUC.bin
echo 3. Create /sdcard/cal
adb shell mkdir /sdcard/cal
echo 4. Push tar file to /sdcard/cal
adb push %CHIPID%.tar /sdcard/cal
if ERRORLEVEL 1 goto exit
echo 5. Remove frame*.raw files
adb shell rm /sdcard/cal/%CHIPID%/frame*.raw
echo 6. Extract tar file to folder
adb root
adb shell "cd /sdcard/cal; tar -xf %CHIPID%.tar"
if ERRORLEVEL 1 goto exit
rm -f %CHIPID%.tar
echo 7. Write CmdWord.bin and RDAC.bin to Maxim
adb shell "cd /sdcard/cal/%CHIPID%; /data/local/tmp/spiutil -a8000 -fCmdWord.bin -fCmdWord.bin -fRDAC.bin /dev/%SPIDEV%"
if ERRORLEVEL 1 goto exit
echo 8. Remove %CHIPID%.tar file
adb shell rm /sdcard/cal/%CHIPID%.tar
:exit
