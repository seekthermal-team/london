#!/bin/bash
# This script runs spiread to read the telemetry line from the sensor
# and then parse out the 48-bit ChipId into the CHIPID env variable

#set -x				# uncomment for debugging

if [ -z $1 ]
then
    SPIDEV=spidev1.0
else
    SPIDEV=$1
fi

#
# 1. read ROIC & CHIPID from sensor using spiutil
# 2. create tar file from CHIPID directory
# 3. create /sdcard/cal folder
# 4. push tar file to /sdcard/cal
# 5. remove frame*.raw
# 6. remove FlatField.bin
# 7. extract tar file to folder
# 8. write CmdWord.bin & RDAC.bin to Maxim using spiutil
# 9. remove tar file
#
TELEM=$(adb shell /data/local/tmp/spiutil -w416 /dev/$SPIDEV)
ROIC=${TELEM:2:2}${TELEM:0:2}
CHIP0=${TELEM:336:2}
CHIP1=${TELEM:340:2}
CHIP2=${TELEM:344:2}
CHIP3=${TELEM:348:2}
CHIP4=${TELEM:352:2}
CHIP5=${TELEM:356:2}
PRODUCT=${TELEM:192:2}
VARIANT=${TELEM:196:2}
MAJOR=${TELEM:200:2}
MINOR=${TELEM:204:2}
export CHIPID=$CHIP5$CHIP4$CHIP3$CHIP2$CHIP1$CHIP0
echo "1. read ROIC is" $ROIC " Version is" $((0x${PRODUCT})).$((0x${VARIANT})).$((0x${MAJOR})).$((0x${MINOR})) " ChipId is" $CHIPID

if [ ! -d "$CHIPID" ]; then
    echo "ERROR Directory" $CHIPID "does not exist!"
    exit -1
fi


echo "2. Creating" $CHIPID.tar"..."
rm -f $CHIPID.tar
COPYFILE_DISABLE=1 tar -cf $CHIPID.tar $CHIPID/AthermLo.bin $CHIPID/Atherm.bin $CHIPID/AthermHi.bin $CHIPID/CintLut.bin $CHIPID/CmdWord.bin $CHIPID/RDAC.bin $CHIPID/FactorySettings.bin $CHIPID/HG_Delta.bin $CHIPID/LG_Delta.bin $CHIPID/ThermAdjust.bin $CHIPID/ThermAdjust_FF.bin $CHIPID/ThermHgKa.bin $CHIPID/ThermHgKm.bin $CHIPID/IP_SBNUC.bin
echo "3. Create /sdcard/cal"
adb shell mkdir /sdcard/cal
echo "4. Push tar file to /sdcard/cal"
adb push $CHIPID.tar /sdcard/cal
if [ "$?" -ne 0 ]; then exit 1; fi
echo "5. Remove frame*.raw files"
adb shell rm /sdcard/cal/$CHIPID/frame*.raw
echo "6. Remove IP_SBNUC*.bin init*.bin files"
adb shell rm /sdcard/cal/$CHIPID/IP_SBNUC*.bin /sdcard/cal/$CHIPID/init*.bin
echo "7. Extract tar file to folder"
adb root
adb shell "cd /sdcard/cal; tar -xf $CHIPID.tar"
if [ "$?" -ne 0 ]; then exit 1; fi
echo "8. Write CmdWord.bin & RDAC.bin to Maxim"
adb shell "cd /sdcard/cal/$CHIPID; /data/local/tmp/spiutil -a8000 -fCmdWord.bin -fCmdWord.bin -fRDAC.bin /dev/$SPIDEV"
if [ "$?" -ne 0 ]; then exit 1; fi
echo "9. Remove" $CHIPID.tar "file"
adb shell rm /sdcard/cal/$CHIPID.tar
rm -f $CHIPID.tar
