# London 32K Public Binary repository

This is a Seek Thermal public repository containing (binary) files for the London project.
Therefore only non-ITAR binary images should be stored here...

## FPA Calibration Files

Each FPA Sensor Data is stored in a separate directory matching the 48-bit (12-digit Hex) ChipID:

* Atherm.bin
* AthermHi.bin
* AthermLo.bin
* CintLut.bin
* CmdWord.bin
* FactorySettings.bin
* HG_Delta.bin
* LG_Delta.bin
* RDAC.bin
* ThermAdjust.bin
* ThermHgKa.bin
* ThermHgKm.bin
* ThermLgKa.bin
* ThermLgKm.bin

## Executable Binaries

Binary executables are stored in the /bin directory:

* SPIDEMO.apk - Android SPIDEMO Debug Application (adb install -t SPIDEMO.apk)
* SPIDMA.bin - Maxim 32660 Flash Code (Program into flash using ./progcode.sh)
