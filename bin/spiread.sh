#!/bin/bash
# This script runs spiutil to read Flash 0x8000

set -x				# uncomment for debugging
if [ -x $1 ]
then
    SPIDEV=spidev2.0
else
    SPIDEV=$1
fi

if [ -x $2 ]
then
    ADDR=00008000
else
    ADDR=$2
fi

adb shell /data/local/tmp/spiutil -n -c3412785601010400${ADDR:2:2}${ADDR:0:2}${ADDR:6:2}${ADDR:4:2} -w416 /dev/$SPIDEV
