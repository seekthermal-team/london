#!/bin/bash
# This script run openocd to program SPIDMA.bin into Flash @ offset 0x0000
# Developed using info from the following webpage:
#       http://openocd.org/doc/html/Flash-Commands.html#program
set -x

if [ -x $1 ]
then
    FIRMWARE=SPIDMA.bin
else
    FIRMWARE=$1
fi

~/Maxim/Toolchain//bin/openocd -s ~/Maxim/Toolchain/share/openocd/scripts -f interface/cmsis-dap.cfg -f target/max32660.cfg -c "program $FIRMWARE verify exit 0x0000000"
