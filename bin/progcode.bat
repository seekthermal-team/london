REM
REM This batch file run openocd to program SPIDMA.bin into Flash @ offset 0x0000
REM Developed using info from the following webpage:
REM       http://openocd.org/doc/html/Flash-Commands.html#program

if not .%1 == . goto else
    set FIRMWARE=SPIDMA.bin
    goto endif
:else
    FIRMWARE=%1
:endif

set MAXIM=C:\Maxim\Toolchain

%MAXIM%\bin\openocd -s %MAXIM%\share\openocd\scripts -f interface/cmsis-dap.cfg -f target/max32660.cfg -c "program %FIRMWARE% verify exit 0x0000000"
