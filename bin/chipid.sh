#!/bin/bash
# This script runs spiread to read the telemetry line from the sensor
# and then parse out the 48-bit ChipId into the CHIPID env variable

#set -x				# uncomment for debugging

if [ -x $1 ]
then
    SPIDEV=spidev2.0
else
    SPIDEV=$1
fi

TELEM=$(adb shell /data/local/tmp/spiutil -c3412785600000000 -w416 /dev/$SPIDEV)
ROIC=${TELEM:2:2}${TELEM:0:2}
CHIP0=${TELEM:336:2}
CHIP1=${TELEM:340:2}
CHIP2=${TELEM:344:2}
CHIP3=${TELEM:348:2}
CHIP4=${TELEM:352:2}
CHIP5=${TELEM:356:2}
PRODUCT=${TELEM:192:2}
VARIANT=${TELEM:196:2}
MAJOR=${TELEM:200:2}
MINOR=${TELEM:204:2}
export CHIPID=$CHIP5$CHIP4$CHIP3$CHIP2$CHIP1$CHIP0
echo "ROIC is" $ROIC " Version is" $((0x${PRODUCT})).$((0x${VARIANT})).$((0x${MAJOR})).$((0x${MINOR})) " ChipId is" $CHIPID
