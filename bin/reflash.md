Instructions for flashing FIH android board

## Tool Versions (from platform-tools 28.0.1)

- Android Debug Bridge version 1.0.40 Version 4986621
- fastboot version 28.0.1-4986621

## Steps

cd ~/london/bin

### 1) Force board into fastboot mode
sudo adb reboot bootloader

### 2) Reflash the image(s)
sudo fastboot flash boot boot.img

### 3) Reflash recovery image
sudo fastboot flash recovery recovery.img

#### 4) Reflash system image
sudo fastboot flash system system.img

#### 5) Reflash persist image
sudo fastboot flash persist persist.img

#### 6) Reflash cache image
sudo fastboot flash cache cache.img

#### 7) Reflash userdata image
sudo fastboot flash userdata userdata.img

### 8) Reboot to new image
sudo fastboot reboot #<-- stop copy here


Instructions for flashing Maxim 32660 on FIH board

## Tool Versions


## Steps

### 1) Power FIH board from 4.00V @ 2.00A
### 2) Turn on 2.8V power to Maxim

sudo adb shell

## 3) Run progcode.sh in london/bin folder to program Maxim latest 22.8.0.113 firmware

''''
#!/bin/bash
# This script run openocd to program caldata.bin into Flash @ offset 0x8000
# Developed using info from the following webpage:
#       http://openocd.org/doc/html/Flash-Commands.html#program
~/Maxim/Toolchain//bin/openocd -s ~/Maxim/Toolchain/share/openocd/scripts -f interface/cmsis-dap.cfg -f target/max32660.cfg -c "program SPIDMA.bin verify exit 0x0000000"
''''

Power Up FIH board with no touch screen

Set Power Supply to 4.00V 2.00A current limit

adb shell input keyevent 82
adb shell input keyevent 82
adb shell input keyevent KEYCODE_TAB
adb shell input keyevent KEYCODE_TAB
adb shell input keyevent KEYCODE_ENTER

Expected current < 1000mA

## To remove SPIDEMO
adb root
adb disable-verity
adb reboot
adb remount
adb shell
cd /system/app
rm -rf seek_app
reboot

## Instal new SPIDEMO
adb install -t SPIDEMO.apk
adb install seekware_fihdemo_20190115_no_raw_dump.apk 
