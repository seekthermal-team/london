#!/bin/bash
# This script runs spiutil to write caldata.bin to Flash 0x8000

set -x				# uncomment for debugging
if [ -x $1 ]
then
    SPIDEV=spidev2.0
else
    SPIDEV=$1
fi

adb shell "cd /sdcard/cal; /data/local/tmp/spiutil -n -a8000 -fcaldata.bin /dev/$SPIDEV"
