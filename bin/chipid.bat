@echo off
REM
REM This DOS batch script reads a telemetry line from the sensor and then
REM parses out the 48-bit ChipId into the CHIPID env variable
REM
if .%1 == . goto default
set SPIDEV=%1
goto skipset
:default
set SPIDEV=spidev1.0
:skipset
adb shell /data/local/tmp/spiutil -w416 /dev/%SPIDEV% > telem.txt
set /p TELEM=<telem.txt
set ROIC=%TELEM:~2,2%%TELEM:~0,2%
set CHIP0=%TELEM:~336,2%
set CHIP1=%TELEM:~340,2%
set CHIP2=%TELEM:~344,2%
set CHIP3=%TELEM:~348,2%
set CHIP4=%TELEM:~352,2%
set CHIP5=%TELEM:~356,2%
set PRODUCT=%TELEM:~192,2%
set VARIANT=%TELEM:~196,2%
set MAJOR=%TELEM:~200,2%
set MINOR=%TELEM:~204,2%
set CHIPID=%CHIP5%%CHIP4%%CHIP3%%CHIP2%%CHIP1%%CHIP0%
echo 1. read ROIC is %ROIC%  Version is %PRODUCT%.%VARIANT%.%MAJOR%.%MINOR%  ChipId is %CHIPID%
