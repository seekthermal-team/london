#!/bin/bash
# This script runs openocd to erase the cal data from Flash @ offset 0x08000
# Developed using info from the following webpage:
#       http://openocd.org/doc/html/Flash-Commands.html#program
~/Maxim/Toolchain//bin/openocd -s ~/Maxim/Toolchain/share/openocd/scripts -f interface/cmsis-dap.cfg -f target/max32660.cfg -c "flash erase_address 0x08000 0x38000; exit"
