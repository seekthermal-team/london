#!/bin/bash
# This script runs spiutil to read a frame into frameNNNN.bin
if [ -x $1 ]
then
    SPIDEV=spidev2.0
else
    SPIDEV=$1
fi

if [ -x $2 ]
then
    LOOPS=10
else
    LOOPS=$2
fi

# Set formatbits=16
adb shell /data/local/tmp/spiutil -n -c34127856060002001000 -w416 /dev/$SPIDEV

for (( num=0; num < $LOOPS; num++ ))
do
	frame=$(printf "%04d" $num)
	echo -n "frame$frame.bin: "
	adb shell /data/local/tmp/spiutil -n -w416 -h157 -r /dev/$SPIDEV > frame$frame.bin
	adb shell /data/local/tmp/spiutil -n -w16 /dev/$SPIDEV
done
