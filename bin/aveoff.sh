#!/bin/bash
# This script runs spiutil to turn off the averaging filter

set -x				# uncomment for debugging
if [ -x $1 ]
then
    SPIDEV=spidev2.0
else
    SPIDEV=$1
fi

adb shell /data/local/tmp/spiutil -n -c34127856030002000000 -w416 /dev/$SPIDEV
