#!/bin/bash
# This script run openocd to program SPIDMA.bin into Flash @ offset 0x2000
# Developed using info from the following webpage:
#       http://openocd.org/doc/html/Flash-Commands.html#program
~/Maxim/Toolchain//bin/openocd -s ~/Maxim/Toolchain/share/openocd/scripts -f interface/cmsis-dap.cfg -f target/max32660.cfg -c "program SPIDMA.bin verify exit 0x0002000"
