#!/bin/bash
# This script runs spiutil to reset (0xDEADDEAD) the Bootloader

set -x				# uncomment for debugging
if [ -x $1 ]
then
    SPIDEV=spidev2.0
else
    SPIDEV=$1
fi

adb shell /data/local/tmp/spiutil -n -c3412785602000400ADDEADDE -w208 /dev/$SPIDEV
