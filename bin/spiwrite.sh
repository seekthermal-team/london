#!/bin/bash
# This script runs spiutil to program SPIDMA firmware at 0x4000

set -x				# uncomment for debugging
if [ -z $1 ]
then
    SPIDEV=spidev2.0
else
    SPIDEV=$1
fi

if [ -z $2 ]
then
    FIRMWARE=SPIDMA.bin
else
    FIRMWARE=$2
fi

if [ -z $3 ]
then
    ADDR=4000
else
    ADDR=$3
fi

adb push $FIRMWARE /sdcard/cal
adb shell "cd /sdcard/cal;/data/local/tmp/spiutil -a$ADDR -f$FIRMWARE /dev/$SPIDEV"
adb shell rm /sdcard/cal/$FIRMWARE
