#!/bin/bash
# This script runs spiutil to program SPIDMA firmware at 0x20000000

set -x				# uncomment for debugging
if [ -z $1 ]
then
    SPIDEV=spidev2.0
else
    SPIDEV=$1
fi

if [ -z $2 ]
then
    FIRMWARE=SPIRAM.bin
else
    FIRMWARE=$2
fi

if [ -z $3 ]
then
    ADDR=20000000
else
    ADDR=$3
fi

adb push $FIRMWARE /sdcard/cal
adb shell "cd /sdcard/cal;/data/local/tmp/spiutil -x -a$ADDR -f$FIRMWARE /dev/$SPIDEV"
adb shell rm /sdcard/cal/$FIRMWARE
