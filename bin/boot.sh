#!/bin/bash
# This script runs spiutil to boot the Appl code from Flash 0x00002000 to SRAM

set -x				# uncomment for debugging
if [ -x $1 ]
then
    SPIDEV=spidev2.0
else
    SPIDEV=$1
fi

adb shell /data/local/tmp/spiutil -n -c341278560401040000000020 -w208 /dev/$SPIDEV
